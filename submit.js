function sendrequest() {
    name = $("#name-id").val();
    phone = $("#phone-id").val();
    mail = $("#mail-id").val();
    quan = $('input[name=quantity]:checked').val() || $('#quan-id').val();
    color = $('input[name=color]:checked').val();
    layer = $('input[name=layer]:checked').val();
    thickness = $('input[name=thickness]:checked').val();
    copper = $('input[name=copper-thick]:checked').val();
    finish = $('input[name=finish]:checked').val();
    invoice = $('input[name=invoice]:checked').val();
    stencil = $('input[name=stencil]:checked').val();
    data = {
        "name": name,
        "email": mail,
        "phone": phone,
        "quantity": parseInt(quan),
        "color": color,
        "numLayer": layer,
        "density": thickness,
        "densityCopper": copper,
        "platingStyle": finish,
        "bill": invoice=='true',
        "stencil": stencil,
        "note": ""
    }
    console.log(data)
    var xhr = new XMLHttpRequest();
    var url = "http://localhost:3000/baogia";
    //
    xhr.open('POST', url, true);
    xhr.setRequestHeader("Content-Type", "application/json;charset=UTF-8");
    xhr.onload = function () {
        console.log(this.responseText);
    };
    xhr.send(JSON.stringify(data));
}

function quaninput(){
    //console.log($('input[name=quantity]:checked').val());
    $('input[name=quantity]:checked').prop('checked', false);
}

$(document).ready(function(){
$('input[name=quantity]').click(function(e){
    $('#quan-id').val('');
});
});